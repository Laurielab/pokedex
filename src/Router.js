import PokemonDetails from "./PokemonDetails"
import App from "./App"
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";

function Router() {
    return(
        <BrowserRouter>
            <Switch>
                <Route path="/PokemonDetails/:id">
                    <PokemonDetails />
                </Route>
                <Route path="/">
                    <App />
                </Route>
            </Switch>
        </BrowserRouter>
    )
}

export default Router