import React from 'react';
import './App.css';

function Pokemon({pokemon}) {
    const [state, setState] = React.useState({
        loading: true,
        pokemonData: {}
    });

    React.useEffect(() => {
        const effect = async () => {
            const response = await fetch(pokemon.url);
            const data = await response.json();

            setState({
                loading: false,
                pokemonData: data
            })
        }

        effect();
    });

    const loading = state.loading;
    const pokemonData = state.pokemonData;

    return (
        <div id="pokemon"> 
            {loading ? 
                "Loading..."
                : <>
                    <a className="pokemonDetailsLink" href={"/pokemonDetails/" + pokemonData.id}>
                        <span>
                            <img className="smallPokemonImage" src={pokemonData.sprites.front_default} alt="Pokemon"/>
                        </span>
                        <span className="pokemonName">
                            {pokemon.name}
                        </span>
                    </a>
                    <span className="detailsColor pikachuTheme">Types: </span>
                    <span className="pokemonType">{pokemonData.types.map(x => x.type.name).join(", ")}</span>
                </> 
            }
        </div>
    );
}

export default Pokemon;
