import React from 'react';
import {
    useParams
} from "react-router-dom";
  
function FindEvolution(name, evolutionChain){
    var evs;
    if(evolutionChain.species.name === name){
        const regex = new RegExp("https://pokeapi.co/api/v2/pokemon-species/(.*)/");
        evs = evolutionChain.evolves_to.map(x => {
            return {
            name: x.species.name,
            id: x.species.url.match(regex)[1]
            }
        })
    }
    else if(evolutionChain.evolves_to.length === 0){
        return null;
    }else{
        for(let i = 0; i< evolutionChain.evolves_to.length;i++){
            evs = FindEvolution(name, evolutionChain.evolves_to[i]);
            if(evs != null) return evs;
        }
    }
    return evs;
}

function PokemonDetails(){
    let { id } = useParams();

    const [state, setState] = React.useState({
        flavor: "",
        color: "",
        name: "",
        loading: true,
        pokemonData: "",
        evolutions: []
    });
    React.useEffect(() => {
        const effect = async () => {
            const pokemonDetailsReponse = await fetch("https://pokeapi.co/api/v2/pokemon-species/" + id)
            const pokemonDetails = await pokemonDetailsReponse.json();

            const evolutionChainResponse = await fetch(pokemonDetails.evolution_chain.url)
            const evolutionChain = await evolutionChainResponse.json()
            
            const pokemonResponse = await fetch("https://pokeapi.co/api/v2/pokemon/" + pokemonDetails.name);
            const pokemonData = await pokemonResponse.json()
            setState({
                flavor: pokemonDetails.flavor_text_entries.find(x => x.language.name === "en").flavor_text,
                loading: false,
                color: pokemonDetails.color.name,
                name: pokemonDetails.name,
                pokemonData: pokemonData,
                evolutions: FindEvolution(pokemonDetails.name, evolutionChain.chain)
            })
        };
        effect();
    });
    
    const evolutions = state.evolutions;
    const loading = state.loading;
    const name = state.name;
    const color = state.color;
    const pokemonData = state.pokemonData;
    const flavor = state.flavor;

    return (
        <div className="App"> 
            <div className="App-header">
                <div className="pokemonDetails">
                    {loading ? "Loading..." :
                        <>
                            <div className="titleImage">
                                <img className="bigPokemonImage" src={pokemonData.sprites.front_shiny} alt="Pokemon"/>
                                <h1 className="pikachuTheme title">{name}</h1>
                            </div>
                            <div>
                                {flavor}
                            </div>
                            <div>
                                <span className="pikachuTheme detailsColor">Color :</span>
                                <span> {color}</span>
                            </div>
                            <div>
                                <span className="pikachuTheme detailsColor">Type: </span>
                                <span>{pokemonData.types.map(x => x.type.name).join(", ")}</span>
                            </div>
                            <div>
                                <span className="pikachuTheme detailsColor">Evolutions:</span>
                                <ul>
                                    {evolutions.length === 0 ? "No evolution" : 
                                        evolutions.map(x => 
                                            <li>
                                                <a className="evolutionsList" href={"/pokemonDetails/" + x.id} key={x.name}>
                                                    {x.name}
                                                </a>
                                            </li>
                                        )
                                    }
                                </ul>
                            </div>
                        </>
                    }
                </div>
            </div>
        </div>
    );
}

export default PokemonDetails;