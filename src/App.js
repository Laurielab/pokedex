import React from 'react';
import Pokemon from "./Pokemon"
import './App.css';

function App() {
  const [state, setState] = React.useState({
    loading: true,
    pokemons: []
  });
  const [page, setPage] = React.useState(1);
  
  React.useEffect(() => {
    const effect = async () => {
      const offset = (page - 1) * 10;
      const response = await fetch('https://pokeapi.co/api/v2/pokemon?limit=10&offset=' + offset);
      const pokemons = await response.json();

      setState({
        loading: false,
        pokemons: pokemons.results
      })
    }

    effect();
  }, [page]);
  
  const loading = state.loading;
  const pokemons = state.pokemons;
  const maxPageNumber = 15;

  return (
    <div className="App"> 
      <div className="App-header">
        <h1 className="pikachuTheme">My Pokémon</h1>
        <p id="pokemonsList">
          {loading ? 
            "Loading..."
            : pokemons.map(x => <Pokemon key={x.name} pokemon={x}/>)}
        </p>
      </div>
      <div className="pokemonsListPage">
        {page !== 1 ? <button className="paginationButtons" onClick={() => setPage(page - 1)}>Previous Page</button> : null }
        {page < maxPageNumber ? <button className="paginationButtons" onClick={() => setPage(page + 1)}>Next Page</button> : null}
      </div>
    </div>
  );
}

export default App;
